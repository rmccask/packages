# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=glfw
pkgver=3.2.1
pkgrel=0
pkgdesc="OpenGL desktop development library"
url="http://www.glfw.org/"
arch="all"
options="!check"  # Tests require X11, GL, and manual intervention.
license="Zlib"
depends=""
depends_dev="mesa-dev"
makedepends="$depends_dev cmake doxygen libx11-dev libxrandr-dev libxcursor-dev
	libxinerama-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/glfw/glfw/releases/download/3.2.1/glfw-$pkgver.zip"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_SHARED_LIBS=ON \
		-DGLFW_BUILD_TESTS=OFF \
		-DGLFW_BUILD_EXAMPLES=OFF \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	install -D -d "$builddir"/docs/html "$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="73dd6d4a8d28a2b423f0fb25489659c1a845182b7ef09848d4f442cdc489528aea90f43ac84aeedb9d2301c4487f39782b647ee4959e67e83babb838372b980c  glfw-3.2.1.zip"
