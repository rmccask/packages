# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdb
pkgver=3.1.0
pkgrel=0
pkgdesc="Database connectivity and creation framework"
url="https://community.kde.org/KDb"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev"
makedepends="$depends_dev cmake extra-cmake-modules icu-dev python3-dev
	qt5-qttools-dev"
install=""
subpackages="$pkgname-dev $pkgname-doc"
source="http://download.kde.org/stable/kdb/src/kdb-$pkgver.tar.xz"
builddir="$srcdir/kdb-$pkgver"

build() {
	cd "$builddir"
	mkdir build
	cd build
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH=True \
		${CMAKE_CROSSOPTS} \
		..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="f94f1ff87cb5133570f7e9b0ab48202a516eabe1c3f2ed750cd1794800d9b9936bbd62e1479ca6877c293f6473b7f4e2d4a144b928b5bc0f228af327131b43c2  kdb-3.1.0.tar.xz"
