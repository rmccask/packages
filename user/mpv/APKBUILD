# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=mpv
pkgver=0.29.1
pkgrel=2
pkgdesc="An improved fork of mplayer"
url="https://mpv.io"
arch="all"
options="!check" # no tests
license="LGPL-2.1+ AND GPL-2.0+ AND ISC AND BSD-2-Clause AND MIT AND BSD-3-Clause"
makedepends="python3
	zlib-dev libarchive-dev py3-docutils uchardet-dev ncurses-dev
	
	alsa-lib-dev pulseaudio-dev

	libx11-dev libxext-dev libxinerama-dev libxrandr-dev libxscrnsaver-dev
	mesa-dev libva-dev lcms2-dev libvdpau-dev

	ffmpeg-dev libbluray-dev v4l-utils-dev libass-dev libdvdread-dev
	libdvdnav-dev libcdio-dev libcdio-paranoia-dev rubberband-dev
	"
subpackages="$pkgname-doc"
source="mpv-$pkgver.tar.gz::https://github.com/mpv-player/mpv/archive/v$pkgver.tar.gz"

build() {
	python3 ./bootstrap.py
	python3 ./waf configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--destdir="$pkgdir" \
		--disable-lua \
		--disable-javascript \
		--disable-wayland \
		--disable-gl-wayland \
		--enable-dvdread \
		--enable-dvdnav \
		--enable-cdda \
		--enable-tv
	python3 ./waf build
}

package() {
	python3 ./waf install --destdir="$pkgdir"
}

sha512sums="ec57c9ceaaf2915ee237dd5a1c5ea5d22725d8611e28a9b998e5bb0d8ab5bdf3631d0267fc7b54da31cb1eaa145ef35841e68846bd41c3b9e1024902e92fd086  mpv-0.29.1.tar.gz"
