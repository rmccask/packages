# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwidgetsaddons
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework containing ready-made widgets for common tasks"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test suite requires running X11 server (passes on ciall)
# Tests are GPL-2.0+, but not shipped.
license="LGPL-2.1+ AND Unicode-DFS-2016"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwidgetsaddons-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="e52375a10af10bd1134d33e793511fa91c135079d2479686ce0f559e31e2e6a71de311773c6c6353343b8ebd3c29e35aa604402cf6cc0b53169775c3babc6bc8  kwidgetsaddons-5.54.0.tar.xz"
