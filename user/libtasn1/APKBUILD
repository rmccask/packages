# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libtasn1
pkgver=4.14
pkgrel=0
pkgdesc="Highly portable ASN.1 library"
url="https://www.gnu.org/software/libtasn1/"
arch="all"
license="LGPL-2.1+"
makedepends="texinfo"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="ftp://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.gz
	"

# secfixes:
#   4.14-r0:
#     - CVE-2018-1000654
#   4.13-r0:
#     - CVE-2018-6003
#   4.12-r1:
#     - CVE-2017-10790

build() {
	cd "$builddir"
	CFLAGS="-Wno-error=inline" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make -j1
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="Tools for parsing and manipulating ASN.1"
	license="GPL-3.0+"
	mkdir -p "$subpkgdir"/usr
	mv -i "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="efdcf3729e9e057cafbfdc9929f08531de03cf3b64e7db62cb53c26bf34c8db4d73786fd853620ab1a10dbafe55e119ad17bfeb40e191071945c7b4db9c9e223  libtasn1-4.14.tar.gz"
