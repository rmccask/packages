# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=symboleditor
pkgver=2.1.0
pkgrel=0
pkgdesc="Symbol library creator for Qt 5"
url="https://userbase.kde.org/SymbolEditor"
arch="all"
license="GPL-2.0+"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdoctools-dev kconfig-dev
	ki18n-dev kio-dev kwidgetsaddons-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/$pkgname/$pkgver/symboleditor-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="dc83f3f80b12d8217a230d8c951e90bcc6246c4460d987efb140c54c1e88a8f5d607ac7ebfe706399cd0917e22a2089df6742afadd28305d39ceeff0c55da535  symboleditor-2.1.0.tar.xz"
