# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=freetype
pkgver=2.10.1
pkgrel=0
pkgdesc="TrueType font rendering library"
url="https://www.freetype.org/"
arch="all"
license="FTL OR GPL-2.0+"
options="!check"
depends=""
makedepends="zlib-dev libpng-dev bzip2-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="http://download.savannah.gnu.org/releases/freetype/freetype-$pkgver.tar.xz"

# secfixes:
#   2.9.1-r0:
#     - CVE-2018-6942
#   2.7.1-r1:
#     - CVE-2017-8105
#     - CVE-2017-8287

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-static \
		--with-bzip2 \
		--with-png \
		--enable-freetype-config
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

doc() {
	gzip "$pkgdir/usr/share/man/man1/freetype-config.1"
	default_doc
}

sha512sums="c7a565b0ab3dce81927008a6965d5c7540f0dc973fcefdc1677c2e65add8668b4701c2958d25593cb41f706f4488765365d40b93da71dbfa72907394f28b2650  freetype-2.10.1.tar.xz"
