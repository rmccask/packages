# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcachegrind
pkgver=19.08.2
pkgrel=0
pkgdesc="Profile data visualisation tool and call graph viewer"
url="https://kcachegrind.github.io/html/Home.html"
arch="all"
license="GPL-2.0-only"
depends="binutils graphviz"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev
	karchive-dev kconfig-dev kcoreaddons-dev kdoctools-dev ki18n-dev kio-dev
	kwidgetsaddons-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kcachegrind-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="7e2c7dc247e5adce6b68097ae265bbd449634efff581a68a18c725635ac8e976c9e5c92e8dbcde519d57b4fbb904e97afee85407992c8e5b2ab4a9fb04b28208  kcachegrind-19.08.2.tar.xz"
