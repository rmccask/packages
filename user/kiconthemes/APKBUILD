# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kiconthemes
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for icon theming"
url="https://www.kde.org/"
arch="all"
options="!check"  # requires X11 running
license="LGPL-2.1-only"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtsvg-dev karchive-dev ki18n-dev
	kcoreaddons-dev kconfigwidgets-dev kitemviews-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kiconthemes-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="2a2f053cb2cc1cdab4dc30f42e51431e205a75406079df583c97f2ca39911cc39f9dbb6f56b44138242d7a9e54234dc36280b48859bf3867b805d66da16e4454  kiconthemes-5.54.0.tar.xz"
