# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=libqtxdg
pkgver=3.4.0
pkgrel=0
pkgdesc="Qt5-based library implementing the XDG spec"
url="https://lxqt.org"
arch="all"
options="!check"  # Test suite requires X11.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.6.0 qt5-qtbase-dev qt5-qtsvg-dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/libqtxdg/releases/download/$pkgver/libqtxdg-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTS=True \
		-DBUILD_DEV_UTILS=True \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	make test
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="9bd52ae3aa32f8c7de48e562ada1ff2413892c7ac58d5f7a06c0c512a4a6c03e9c5c44930f7781930c9c701894ea38a450a61a3c26afc047c76313ada0a890e6  libqtxdg-3.4.0.tar.xz"
