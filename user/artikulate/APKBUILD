# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=artikulate
pkgver=19.08.2
pkgrel=0
pkgdesc="Pronunciation trainer for languages"
url="https://www.kde.org/applications/education/artikulate/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtxmlpatterns-dev
	karchive-dev kconfig-dev kcrash-dev kdoctools-dev ki18n-dev
	kirigami2-dev knewstuff-dev kxmlgui-dev qt5-qtmultimedia-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/artikulate-$pkgver.tar.xz
	inappropriate-qt.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	# TestCourseFiles needs X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E TestCourseFiles
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="72e4d0bfaa88b4133964f07fe2e20d9f2cc0bee718388fe56f1f1518f75acf31b4068fa87e652c370e18e6b53addce3414430ee43c9118782925c55bb2536dc6  artikulate-19.08.2.tar.xz
dd9a912f7499fee5a7d737f987ba97856e373d48d944d45fb2cfaf192bc5c79f05dc530f0ca62d5982ef86dcf1479211186657ecef0450c07a43527bd66eb7c8  inappropriate-qt.patch"
