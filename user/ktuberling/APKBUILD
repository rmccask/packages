# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ktuberling
pkgver=19.08.2
pkgrel=0
pkgdesc="Simple constructor game"
url="https://games.kde.org/game.php?game=ktuberling"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev ki18n-dev
	kdbusaddons-dev kdelibs4support-dev kwidgetsaddons-dev kxmlgui-dev
	libkdegames-dev qt5-qtmultimedia-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/ktuberling-$pkgver.tar.xz
	frameworks.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="6a5ae8be1804ca90853a0d56cd86f42bc1f6260066d0c6244b6838eb5a86a2d564adae3f0d8def677001dfe3007b2f74831731b57be4b42911a9eb6d0ed28b31  ktuberling-19.08.2.tar.xz
a8c723302d141ff74a10cc38245524d59a51d476159c2ac46a1694747ddcb078abc42cea80cf5a5d3910c96c0c90fb7dbf82786496f03b4438a23a736a784e76  frameworks.patch"
