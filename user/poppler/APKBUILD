# Maintainer: 
pkgname=poppler
pkgver=0.80.0
pkgrel=0
pkgdesc="PDF rendering library based on xpdf 3.0"
url="https://poppler.freedesktop.org/"
arch="all"
options="!check"  # Tests only cover Qt5 component.
license="GPL-2.0+"
depends=""
depends_dev="cairo-dev glib-dev"
makedepends="$depends_dev libjpeg-turbo-dev cairo-dev libxml2-dev openjpeg-dev
	fontconfig-dev gobject-introspection-dev lcms2-dev libpng-dev tiff-dev
	zlib-dev cmake"
subpackages="$pkgname-dev $pkgname-doc $pkgname-utils $pkgname-glib"
source="https://poppler.freedesktop.org/poppler-$pkgver.tar.xz"
builddir="$srcdir"/$pkgname-$pkgver/build

# secfixes:
#   0.77.0-r0:
#     - CVE-2019-9200
#     - CVE-2019-9631
#     - CVE-2019-9903
#     - CVE-2019-10872
#     - CVE-2019-10873
#     - CVE-2019-11026
#     - CVE-2019-12293
#   0.80.0-r0:
#     - CVE-2019-9959
#     - CVE-2019-14494

prepare() {
	default_prepare
	mkdir "$builddir"
}

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DENABLE_UNSTABLE_API_ABI_HEADERS=ON \
		..
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

utils() {
	pkgdesc="Poppler's xpdf-workalike command line utilities"
	install -d "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

glib() {
	pkgdesc="Glib wrapper for poppler"
	replaces="poppler-gtk"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libpoppler-glib.so.* \
		"$pkgdir"/usr/lib/girepository* \
		"$subpkgdir"/usr/lib/
}

sha512sums="0a0d68168ba4d560941de31cb9e32c6cd7b44025e93cd84ace863ffab5b9ff0356524626cb16fb99c29a897738f2ac5862480fc54d42f8aecd2e3457f11c642f  poppler-0.80.0.tar.xz"
