# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=gprbuild
pkgver=2019
_pkgver=2019-20190517-194D8
_xmlver=2019-20190429-19B9D
pkgrel=0
pkgdesc="An advanced build system for multi-language systems"
url="https://github.com/AdaCore/gprbuild"
arch="all"
options="!check" # No test suite.
license="GPL-3.0+"
makedepends="gcc-gnat"
source="$pkgname-$_pkgver-src.tar.gz::http://mirrors.cdn.adacore.com/art/5cdf8e8031e87a8f1d425093
	xmlada-$_xmlver-src.tar.gz::http://mirrors.cdn.adacore.com/art/5cdf916831e87a8f1d4250b5
	foxkit.xml"
builddir="$srcdir/$pkgname-$_pkgver-src"

build() {
	xmlada="../xmlada-$_xmlver-src"
	incflags="-Isrc -Igpr/src -I$xmlada/dom -I$xmlada/input_sources \
	          -I$xmlada/sax -I$xmlada/schema -I$xmlada/unicode"
	gcc -c ${CFLAGS} gpr/src/gpr_imports.c -o gpr_imports.o
	for bin in gprbuild gprconfig gprclean gprinstall gprls gprname; do
		gnatmake -j$JOBS $incflags $ADAFLAGS $bin-main \
			-o $bin -cargs $CFLAGS -largs $LDFLAGS gpr_imports.o
	done
	for lib in gprlib gprbind; do
		gnatmake -j$JOBS $incflags $ADAFLAGS $lib \
			-cargs $CFLAGS -largs $LDFLAGS gpr_imports.o
	done
}

package() {
	mkdir -p "$pkgdir"/usr/bin
	cp gprbuild gprconfig gprclean gprinstall gprls gprname \
		"$pkgdir"/usr/bin
	mkdir -p "$pkgdir"/usr/libexec/gprbuild
	cp gprlib gprbind \
		"$pkgdir"/usr/libexec/gprbuild
	mkdir -p "$pkgdir"/usr/share/gpr
	cp share/_default.gpr \
		"$pkgdir"/usr/share/gpr
	mkdir -p "$pkgdir"/usr/share/gprconfig
	cp share/gprconfig/* "$srcdir"/foxkit.xml \
		"$pkgdir"/usr/share/gprconfig
}

sha512sums="0388d7e6cdf625fef40b33da764091b3283dd88714c64ecfad4b5e99253f183b1a9b717c19281e6d45cc4277a05fadb2e2ace394d6f30638664b66c97123052b  gprbuild-2019-20190517-194D8-src.tar.gz
f8e0e25b0aee9a7a18223ae7761603082af55134f062c767d0cd0dbf0dbcb2058fc7c57532b567fb2c6efa464a53ead57b300578e98962025866e5c3ad73e380  xmlada-2019-20190429-19B9D-src.tar.gz
e369c094963d3dcfb03d7ac0949825531bae6410ef9c4bec774cb0da70d2bd4a784bdec37db5151c0371ce769712ee02fc04f36896ccc8bddcdb585c1ee8dbbc  foxkit.xml"
