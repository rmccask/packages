# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=spectacle
pkgver=19.08.2
pkgrel=0
pkgdesc="Application for capturing desktop screenshots"
url="https://www.kde.org/applications/graphics/spectacle/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	kcoreaddons-dev kwidgetsaddons-dev kdbusaddons-dev knotifications-dev
	kconfig-dev ki18n-dev kio-dev kxmlgui-dev kwindowsystem-dev python3
	kdoctools-dev kdeclarative-dev xcb-util-image-dev xcb-util-cursor-dev
	libxcb-dev xcb-util-renderutil-dev knewstuff-dev libkipi-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/spectacle-$pkgver.tar.xz
	no-wayland.patch
	qt5.9.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a03dbe13aa0e0cea07b6505ead130946233574863fc5994d42a971d8a9ae0a24be5e4dfab38640857d5bb89f33dc3736bad7e87a3a76aa83b450156e8db96723  spectacle-19.08.2.tar.xz
36665d1b7d8b7c8004e457179f03fff1eed3865ee3e558f44f19022b80e39bbb2659de3e7b9affe654d5dbc5077890b38b6b4351f3a43324733749fbd36880ac  no-wayland.patch
781e1611026a45232fa117b90a1520bed380e512ae2e0690785a960f897342284ce0c553d8fb22caf10fd34c3823ee7dacb770815c871808345c1bb05126b74f  qt5.9.patch"
