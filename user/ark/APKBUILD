# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ark
pkgver=19.08.2
pkgrel=0
pkgdesc="Graphical file compression/decompression utility with support for multiple formats"
url="https://utils.kde.org/projects/ark/"
arch="all"
options="!check"  # requires other formats not packaged and manual user input
license="GPL-2.0-only"
depends="lzop shared-mime-info unzip zip"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kconfig-dev
	kcrash-dev kdbusaddons-dev kdoctools-dev ki18n-dev kiconthemes-dev
	kitemmodels-dev kio-dev kservice-dev kparts-dev kpty-dev libarchive-dev
	kwidgetsaddons-dev xz-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/ark-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c4ae42a68a9f00238de4dcfef066c3e20f9a896fe00473b4db4ca4aadc8db1f63c876a98833799c533962283fe3f64be36e563e5624e0dae1978452438392788  ark-19.08.2.tar.xz"
