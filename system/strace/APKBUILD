# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=strace
pkgver=5.3
pkgrel=0
pkgdesc="A useful diagnositic, instructional, and debugging tool"
url="https://strace.io/"
arch="all"
options="!check"  # test suite requires mpers, which requires multilib
license="LGPL-2.1+"
depends=""
makedepends="linux-headers autoconf automake"
subpackages="$pkgname-doc"
source="https://github.com/strace/strace/releases/download/v$pkgver/strace-$pkgver.tar.xz
	fix-ppc-pt-regs-collision.patch
	nlattr-fix.patch
	"

build() {
	case "$CLIBC" in
	musl) export CFLAGS="$CFLAGS -Dsigcontext_struct=sigcontext" ;;
	esac

	case "$CARCH" in
	s390x)
		# __SIGNAL_FRAMESIZE is defined in asm/sigcontext.h
		# but including it would make conflict with struct sigcontext
		# since we compile with it in musl.
		# Temporarily add this until musl upstream has a proper fix
		# for struct sigcontext.
		export CFLAGS="$CFLAGS -D__SIGNAL_FRAMESIZE=160"
		;;
	esac

	ac_cv_have_long_long_off_t=yes \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-mpers
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="02b80a8abedb14e40235709733953005210cd328daba59cb594745e7ed3851d155546ccccdb3e43835aa75eee89f023324e542e15154bbc3269e37fd23433e2a  strace-5.3.tar.xz
64bcd5a335d8117bf6d6eb1e47b47ef5d3df5bdc6e4c52379e0e8b099fd06e5d4fa362b3b88fbacec350783f65de589f2fb562d9df04e2895c8a74c8198e6cea  fix-ppc-pt-regs-collision.patch
6616161b6e015c5e56b7171995d28ab63a865156b7f9826b4be26beaac863f0ebc341014910ea53157a810c6afafc10ce80b2d31f4d649d28718a9be78795c6c  nlattr-fix.patch"
