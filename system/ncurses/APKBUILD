# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=ncurses
pkgver=6.1
_ver=${pkgver}-20190713
pkgrel=0
pkgdesc="Console display library"
url="https://invisible-island.net/ncurses/ncurses.html"
arch="all"
options="!check"  # "tests" are actual demo programs, not a test suite.
license="MIT"
depends=""
makedepends_build="ncurses"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc $pkgname-libs
	$pkgname-terminfo-base:base:noarch $pkgname-terminfo:terminfo:noarch"
source="https://invisible-mirror.net/archives/ncurses/current/ncurses-$_ver.tgz"

builddir="$srcdir"/ncurses-$_ver

# secfixes:
#   6.0_p20171125-r0:
#     - CVE-2017-16879 
#   6.0_p20170701-r0:
#     - CVE-2017-10684

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--libdir=/lib \
		--mandir=/usr/share/man \
		--without-ada \
		--without-tests \
		--disable-termcap \
		--disable-rpath-hack \
		--disable-stripping \
		--with-pkg-config-libdir=/usr/lib/pkgconfig \
		--without-cxx-binding \
		--with-terminfo-dirs="/etc/terminfo:/usr/share/terminfo" \
		--enable-pc-files \
		--with-shared \
		--with-termlib \
		--enable-widec
	make
}

package() {
	make -j1 DESTDIR="$pkgdir" install

	# Install basic terms in /etc/terminfo
	for i in ansi console dumb linux rxvt screen sun vt52 vt100 vt102 \
			vt200 vt220 xterm xterm-color xterm-xfree86; do
		local termfile="$(find "$pkgdir"/usr/share/terminfo/ -name "$i" 2>/dev/null)"
		local basedir="$(basename $(dirname "$termfile"))"

		[ -z "$termfile" ] && continue

		install -d "$pkgdir"/etc/terminfo/$basedir
		mv ${termfile} "$pkgdir"/etc/terminfo/$basedir/
		ln -s ../../../../etc/terminfo/$basedir/$i \
			"$pkgdir"/usr/share/terminfo/$basedir/$i
	done
}

dev() {
	default_dev
	# force link against *w.so and *w.a
	for lib in ncurses ncurses++ form panel menu tinfo; do
		echo "INPUT(-l${lib}w)" > "$subpkgdir"/lib/lib${lib}.so
		echo "INPUT(-l${lib}w)" > "$subpkgdir"/lib/lib${lib}.a
		ln -s ${lib}w.pc "$subpkgdir"/usr/lib/pkgconfig/${lib}.pc
	done
	# link curses -> ncurses
	echo "INPUT(-lncursesw)" > "$subpkgdir"/lib/libcursesw.so
	ln -s libncurses.so "$subpkgdir"/lib/libcurses.so
}

terminfo() {
	pkgdesc="$pkgdesc (other terminfo files)"
	depends="$pkgname-terminfo-base"
	rm -rf $subpkgdir
	mkdir -p $subpkgdir/usr/share
	mv $pkgdir/usr/share/terminfo $subpkgdir/usr/share
}

libs() {
	pkgdesc="$pkgdesc (libraries)"
	depends="$pkgname-terminfo"
	provides="ncurses-widec-libs=$pkgver-r$pkgrel"

	mkdir -p "$subpkgdir"
	mv "$pkgdir"/lib "$subpkgdir"/
}

base() {
	pkgdesc="Descriptions of common terminals"
	mkdir -p "$subpkgdir"/etc
	mv "$pkgdir"/etc/terminfo "$subpkgdir"/etc/
}

static() {
	pkgdesc="Static libraries for the ncurses library"
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/*.a "$subpkgdir"/lib/
}

sha512sums="b13ab1c44f9c5d83dcf9a5d57770450557c4223d22ea05406d640004b1ab1fa97c71d16137b04c85e964d893caae5eaba7567f024b5de95eed56ecbb939218fe  ncurses-6.1-20190713.tgz"
